## [2.3.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postfix/compare/v2.3.2...v2.3.3) (2025-03-10)


### Bug Fixes

* **postfix:** Add recipient delimiter support ([019190d](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postfix/commit/019190d2dd48b6c765c9ea6e553b0b6ad94b8816))

## [2.3.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postfix/compare/v2.3.1...v2.3.2) (2025-03-04)


### Bug Fixes

* **postfix:** Add static auth support ([6c0e59b](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postfix/commit/6c0e59be5e1bd2867aaf7985d60e56c9c3af505a))

## [2.3.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postfix/compare/v2.3.0...v2.3.1) (2025-02-18)


### Bug Fixes

* **postfix:** Fix overrides, add multiple domains ([262a029](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postfix/commit/262a02986e56b22a21f1259ebb81c0fa30993e71))

# [2.3.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postfix/compare/v2.2.0...v2.3.0) (2024-08-07)


### Features

* **postfix:** Place configuration inside helm chart and use postfix 2.0.0 ([948e6c1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postfix/commit/948e6c1d0fc4998a065501df03be3f55ed39276a))

# [2.2.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postfix/compare/v2.1.0...v2.2.0) (2024-07-23)


### Features

* **postfix:** Add configmap checksum check to Postfix Helm chart ([e55c9ef](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postfix/commit/e55c9efb94922a9f208b158a8285db95d85429e2))

# [2.1.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postfix/compare/v2.0.7...v2.1.0) (2024-05-05)


### Features

* add parameter to control the trafficPolicy for the external service ([0a18d7c](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postfix/commit/0a18d7c0330ea0e9bdc7d21b3816f1717b8b04f3))

## [2.0.7](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postfix/compare/v2.0.6...v2.0.7) (2023-12-21)


### Bug Fixes

* Add GPG key, update README.md ([fcdc028](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postfix/commit/fcdc028cc7e68cf9631bf93a6a51326774950209))

## [2.0.6](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postfix/compare/v2.0.5...v2.0.6) (2023-12-20)


### Bug Fixes

* **ci:** Add .common:tags: [] ([5cd8dc3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postfix/commit/5cd8dc3fb5398b3a1fc6343902726ee1ada96185))

## [2.0.5](https://gitlab.opencode.de/bmi/opendesk/components/charts/opendesk-postfix/compare/v2.0.4...v2.0.5) (2023-12-20)


### Bug Fixes

* **ci:** Move repo to Open CoDE ([44a60dc](https://gitlab.opencode.de/bmi/opendesk/components/charts/opendesk-postfix/commit/44a60dc07205533aaa27529c4855d1c1baa3c4e8))

## [2.0.4](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/compare/v2.0.3...v2.0.4) (2023-10-16)


### Bug Fixes

* **postfix:** Use | quote wherever possible ([0230342](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/commit/0230342f06be5dc7da2b6d9fff7d0ddff3e24f73))

## [2.0.3](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/compare/v2.0.2...v2.0.3) (2023-09-12)


### Bug Fixes

* **postfix:** Remove containerSecurityContext capabilites ([85e0eaa](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/commit/85e0eaa401550a40e419de3fdafc6469a0ce029a))

## [2.0.2](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/compare/v2.0.1...v2.0.2) (2023-09-11)


### Bug Fixes

* **postfix:** Set allowPrivilegeEscalation to true ([7403b65](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/commit/7403b650d270ccb2a57493ba471301e775ffbebd))

## [2.0.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/compare/v2.0.0...v2.0.1) (2023-09-05)


### Bug Fixes

* **postfix:** Add missing NET_ADMIN and NET_RAW capabilities ([85d946b](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/commit/85d946b3594bdb56dea32093cd70f6aaf9d653cc))

# [2.0.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/compare/v1.13.0...v2.0.0) (2023-09-03)


### Features

* **postfix:** Improve default security settings ([fcaca4a](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/commit/fcaca4aee013e3e996502dc2494c85bdcebdbe70))


### BREAKING CHANGES

* **postfix:** Remove image.digest and add digest to tag

# [1.13.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/compare/v1.12.0...v1.13.0) (2023-08-09)


### Bug Fixes

* **postfix:** fix typo ([e7f0114](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/commit/e7f011452498cbc1d4de2cd641f1b2a99ff715c3))


### Features

* **postfix:** add note in case image tag and a digest is specified ([b9e4b99](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/commit/b9e4b99cecd115704e747268b983f096b49102fd))
* **postfix:** Make internalDomain empty by default ([6ba5732](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/commit/6ba5732fb91c5eeff0d99c5dbfc2f6ced7e33ccf))

# [1.12.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/compare/v1.11.0...v1.12.0) (2023-08-09)


### Features

* **postfix:** add smtp_host_lookup ([3275d16](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/commit/3275d167d8dd1fd0c2f3b2c64fbba38d0b81e2de))

# [1.11.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/compare/v1.10.0...v1.11.0) (2023-08-09)


### Features

* **postfix:** use digest instead of sha256 ([fd46c56](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/commit/fd46c56470952fc0d8e7ec0c12ebf8d22c0d5c7f))

# [1.10.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/compare/v1.9.2...v1.10.0) (2023-08-08)


### Features

* Support sha256 sum for images ([28e3054](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/commit/28e3054735d0546ce8e05088fc3bba88020f348f))

## [1.9.2](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/compare/v1.9.1...v1.9.2) (2023-08-08)


### Bug Fixes

* Lookup internal k8s services without fqdn ([f6111f6](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/commit/f6111f669fba8c230e8675b940bb2845516e75d9))

## [1.9.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/compare/v1.9.0...v1.9.1) (2023-08-07)


### Bug Fixes

* Merge Lifecycle Hooks when overwritten ([0c4053f](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/commit/0c4053f47627951216af47fa4c08f6c99e6348b0))

# [1.9.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/compare/v1.8.0...v1.9.0) (2023-08-07)


### Features

* Set default updateStrategy to Recreate ([0ea3f4c](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/commit/0ea3f4cdf2fb1470cb6ad3c868a210d88b2d73f0))

# [1.8.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/compare/v1.7.1...v1.8.0) (2023-07-28)


### Features

* **postfix:** Update StorageClass & keeOnDelete ([d0adaa1](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/commit/d0adaa18ba47b363b0e44b0a9ffb760ecb72e424))

## [1.7.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/compare/v1.7.0...v1.7.1) (2023-07-25)


### Bug Fixes

* yamllint ([0d44630](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/commit/0d44630f1a9ae89e5195161c8c939bedf2b28b21))

# [1.7.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/compare/v1.6.2...v1.7.0) (2023-07-25)


### Features

* Add LoadBalancer support & licenses ([756007c](https://gitlab.souvap-univention.de/souvap/tooling/charts/postfix/commit/756007c3a1b62114877530004471827a63216041))
