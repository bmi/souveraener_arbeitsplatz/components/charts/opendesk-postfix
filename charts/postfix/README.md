<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"

SPDX-License-Identifier: Apache-2.0
-->
# postfix

A Helm chart for deploying the Postfix mail server

Postfix is used to send and receive email within the sovereign workplace.
Every environment (= namespace) consists of one Postfix component which can be used by each component to send email.
Received email is normally delivered to the Dovecot component residing in the same namespace.
To be able to receive email from the internet, Postfix needs to be reachable from outside the cluster.

To [make receiving email from the internet](#making-postfix-available-on-the-internet) easier, especially if there are multiple environments in a k8s cluster, a "Global Postfix" can be deployed additionally.
This "Global Postfix" watches all namespaces for dovecot instances and will then automatically deliver incoming email to the right namespace.
Currently, this "Global Postfix" will only work, if all environments share the same base domain and differ only by the namespace, which has to be part of the domain, e.g. `environment1.domain.tld`, `environment2.domain.tld`, and so on.
In order to activate this "namespace watcher", it needs to be enabled (`namespaceWatcher.enabled: true`) and the base domain needs to be set (`namespaceWatcher.domain: "domain.tld"`).

## Installing the Chart

To install the chart with the release name `my-release`:

```console
helm repo add opendesk-postfix https://gitlab.opencode.de/api/v4/projects/1388/packages/helm/stable
helm install my-release opendesk-postfix/postfix
```

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitnami.com/bitnami | common | ^2.x.x |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` | Affinity for pod assignment Ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity Note: podAffinityPreset, podAntiAffinityPreset, and  nodeAffinityPreset will be ignored when it's set |
| autoscaling.enabled | bool | `false` | Enable Horizontal Pod Autoscaling |
| autoscaling.maxReplicas | int | `100` | Maximum amount of Replicas |
| autoscaling.minReplicas | int | `1` | Minimum amount of Replicas |
| autoscaling.targetCPUUtilizationPercentage | int | `80` | set the CPU Utilization Percentage |
| autoscaling.targetMemoryUtilizationPercentage | int | `80` | set the Memory Utilization Percentage |
| certificate.request.enabled | bool | `true` | enable the request of the certificate |
| certificate.request.issuerRef.kind | string | `"ClusterIssuer"` | We can reference ClusterIssuers by changing the kind here. The default value is Issuer (i.e. a locally namespaced Issuer) |
| certificate.request.issuerRef.name | string | `"letsencrypt-prod"` | name of the isuerRef |
| certificate.secretName | string | `""` |  |
| cleanup.keepCertificateOnDelete | bool | `true` | keep TLS Certificate on delete |
| cleanup.keepExternalServiceOnDelete | bool | `true` | Keep external Service on delete (to keep LoadBalancer IP) |
| cleanup.keepPVCOnDelete | bool | `false` | Keep persistence on delete of this release. |
| commonAnnotations | object | `{}` | Additional custom annotations to add to all deployed objects. |
| commonLabels | object | `{}` | Additional custom labels to add to all deployed objects. |
| configSizeLimit | string | `"100Mi"` | Maximum size limit of config directory |
| containerSecurityContext.allowPrivilegeEscalation | bool | `true` | Enable container privileged escalation. |
| containerSecurityContext.capabilities | object | `{}` | Security capabilities for container. |
| containerSecurityContext.enabled | bool | `true` | Enable security context. |
| containerSecurityContext.readOnlyRootFilesystem | bool | `true` | Mounts the container's root filesystem as read-only. |
| containerSecurityContext.runAsNonRoot | bool | `false` | Run container as user. |
| containerSecurityContext.seccompProfile.type | string | `"RuntimeDefault"` | Disallow custom Seccomp profile by setting it to RuntimeDefault. |
| extraEnvVars | list | `[]` | Array with extra environment variables to add to containers.  extraEnvVars:   - name: FOO     value: "bar"  |
| extraVolumeMounts | list | `[]` | Optionally specify extra list of additional volumeMounts. |
| extraVolumes | list | `[]` | Optionally specify extra list of additional volumes |
| fullnameOverride | string | `""` | Provide a name to substitute for the full names of resources. |
| global.imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| global.registry | string | `"docker.io"` | Container registry address. |
| image.imagePullPolicy | string | `"IfNotPresent"` | Define an ImagePullPolicy.  Ref.: https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy  "IfNotPresent" => The image is pulled only if it is not already present locally. "Always" => Every time the kubelet launches a container, the kubelet queries the container image registry to             resolve the name to an image digest. If the kubelet has a container image with that exact digest cached             locally, the kubelet uses its cached image; otherwise, the kubelet pulls the image with the resolved             digest, and uses that image to launch the container. "Never" => The kubelet does not try fetching the image. If the image is somehow already present locally, the            kubelet attempts to start the container; otherwise, startup fails  |
| image.registry | string | `"registry.opencode.de"` | Container registry address. This setting has higher precedence than global.registry. |
| image.repository | string | `"bmi/opendesk/components/platform-development/images/postfix"` | Container repository string. |
| image.tag | string | `"3.0.1@sha256:d2c6543b35b616ac3e6c8c27222d3154c0d35680813a8942ce0cc3fa9ea72a6d"` | Overrides the image tag whose default is the chart appVersion. Please use <TAG>@sha256:<DIGEST> |
| imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| lifecycleHooks | object | `{}` | Lifecycle to automate configuration before or after startup |
| livenessProbe.enabled | bool | `true` | Enables kubernetes LivenessProbe. |
| livenessProbe.failureThreshold | int | `10` | Number of failed executions until container is terminated. |
| livenessProbe.initialDelaySeconds | int | `15` | Delay after container start until LivenessProbe is executed. |
| livenessProbe.periodSeconds | int | `20` | Time between probe executions. |
| livenessProbe.successThreshold | int | `1` | Number of successful executions after failed ones until container is marked healthy. |
| livenessProbe.timeoutSeconds | int | `5` | Timeout for command return. |
| nameOverride | string | `""` | String to partially override release name. |
| namespaceWatcher.delay | string | `"10"` | delay between checks for namespace updates |
| namespaceWatcher.domain | string | `""` | destination domains in the transport.map will be built like: <NAMESPACE>.<DOMAIN> |
| namespaceWatcher.enabled | bool | `false` | The namespaceWatcher is only used, when this Postfix is acting as a "global" Postfix. The "global" Postfix should deliever incoming mail to the Dovecot service running in the correct namespace. E.g. Mail intendend for mail@nightly.develop.souvap-univention.de should be delievered to the Dovecot running in the "nightly" namespace. The namespaceWatcher is a sidecar, which will watch all namespaces with a Dovecot pod. Whenever these namespaces change, it updates the transport.map, runs postmap and reloads the "global" Postfix using kubectl. The transport map will look like this: milestone.develop.souvap-univention.de lmtps:dovecot.milestone.svc.cluster.local:24 nightly.develop.souvap-univention.de lmtps:dovecot.nightly.svc.cluster.local:24 |
| namespaceWatcher.image.imagePullPolicy | string | `"IfNotPresent"` | Define an ImagePullPolicy.  Ref.: https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy  "IfNotPresent" => The image is pulled only if it is not already present locally. "Always" => Every time the kubelet launches a container, the kubelet queries the container image registry to             resolve the name to an image digest. If the kubelet has a container image with that exact digest             cached locally, the kubelet uses its cached image; otherwise, the kubelet pulls the image with the             resolved digest, and uses that image to launch the container. "Never" => The kubelet does not try fetching the image. If the image is somehow already present locally, the            kubelet attempts to start the container; otherwise, startup fails  |
| namespaceWatcher.image.registry | string | `""` | Container registry address. This setting has higher precedence than global.registry. |
| namespaceWatcher.image.repository | string | `"alpine/k8s"` | Container repository string. |
| namespaceWatcher.image.tag | string | `"1.27.6@sha256:8f84a15bd902522a4fa1634e4334a57b419d2ca220c690964159c98fea3032bb"` | Define image tag, please use <TAG>@sha256:<DIGEST> |
| namespaceWatcher.imagePullSecrets | list | `[]` | pullSecrets |
| namespaceWatcher.internalDomain | string | `""` | internal k8s domain if specified, it will be added to destinations in the transport.map e.g. instead of dovecot.namespace the destination would be dovecot.namespace.svc.cluster.local |
| namespaceWatcher.lifecycleHook | object | `{"postStart":{"exec":{"command":["/bin/sh","-c","while ! postfix status; do sleep 1; done"]}}}` | Lifesycle Hook that namespace watcher waits for postfix to be up |
| namespaceWatcher.mailserver | string | `"dovecot"` | mailserver for the found namespace, will be <mailserver>.<NAMESPACE>.<internalDomain> |
| namespaceWatcher.mailserverPort | string | `"24"` | Mailserver port, lmtps defaults to 24 |
| namespaceWatcher.mailserverProtocol | string | `"lmtps"` | Mailserver protocol, dovecot uses lmtps |
| namespaceWatcher.transportMap | string | `"/etc/postfix/transport.map"` | transport.map which the "global" postfix reads |
| namespaceWatcher.workdir | string | `"/namespace-watcher"` | shared working dir between the "global" postfix and this sidecar |
| nodeSelector | object | `{}` | Node labels for pod assignment Ref: https://kubernetes.io/docs/user-guide/node-selection/ |
| persistence.accessModes | list | `["ReadWriteOnce"]` | The volume access modes, some of "ReadWriteOnce", "ReadOnlyMany", "ReadWriteMany", "ReadWriteOncePod".  "ReadWriteOnce" => The volume can be mounted as read-write by a single node. ReadWriteOnce access mode still can                    allow multiple pods to access the volume when the pods are running on the same node. "ReadOnlyMany" => The volume can be mounted as read-only by many nodes. "ReadWriteMany" => The volume can be mounted as read-write by many nodes. "ReadWriteOncePod" => The volume can be mounted as read-write by a single Pod. Use ReadWriteOncePod access mode if                       you want to ensure that only one pod across whole cluster can read that PVC or write to it.  |
| persistence.annotations | object | `{}` | Annotations for the PVC. |
| persistence.dataSource | object | `{}` | Custom PVC data source. |
| persistence.enabled | bool | `true` | Enable data persistence (true) or use temporary storage (false). |
| persistence.labels | object | `{}` | Labels for the PVC. |
| persistence.selector | object | `{}` | Selector to match an existing Persistent Volume (this value is evaluated as a template)  selector:   matchLabels:     app: my-app  |
| persistence.size | string | `"1Gi"` |  |
| persistence.storageClass | string | `""` | The (storage) class of PV. |
| podAnnotations | object | `{}` | Pod Annotations. Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/ |
| podLabels | object | `{}` | Pod Labels. Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/ |
| podSecurityContext.enabled | bool | `true` | Enable security context. |
| podSecurityContext.fsGroup | int | `101` | If specified, all processes of the container are also part of the supplementary group. |
| postfix.amavisNetsIn | string | `""` | Adds additional trusted networks to accept Amavis input. |
| postfix.amavisPortIn | string | `""` | Port to receive Amavis messages, if set postfix will listen on the specified port. |
| postfix.contentFilter | string | `""` | https://www.postfix.org/postconf.5.html#content_filter, e.g. `smtp-amavis:[127.0.0.1]:10024` |
| postfix.dkimpyHost | string | `""` | if dkimpy is used, this should be set to the dkimpy host and port, e.g. `dkimpy.mydomain:8892` |
| postfix.domain | string | `"localdomain"` | https://www.postfix.org/postconf.5.html#mydomain, default `localdomain` |
| postfix.hostname | string | `"localhost"` | https://www.postfix.org/postconf.5.html#myhostname, default `localhost` |
| postfix.inetProtocols | string | `"all"` | https://www.postfix.org/postconf.5.html#inet_protocols, can be ipv4, ipv6 or all, default: `all` |
| postfix.lmtpHostLookup | string | `"native"` | https://www.postfix.org/postconf.5.html#lmtp_host_lookup), default `native` (postfix default is `dns`) |
| postfix.messageSizeLimit | string | `"50000000"` | https://www.postfix.org/postconf.5.html#message_size_limit in bytes, default `50000000` |
| postfix.milterDefaultAction | string | `"accept"` | https://www.postfix.org/postconf.5.html#milter_default_action, default `accept` |
| postfix.myDestination | string | `"$myhostname, localhost.$mydomain, localhost"` | https://www.postfix.org/postconf.5.html#mydestination, default `$myhostname, localhost.$mydomain, localhost` |
| postfix.overrideMaps | list | `[]` | overrideMaps can be used to mount specific files from configmaps: - name: "configmapname"   key:  "this.conf"   mountPath: "/mnt/me/there.conf" |
| postfix.overrides | list | `[]` | set postfix.overrides[0].content[0]="mail.example relayhost_user:relayhost_password" |
| postfix.recipientDelimiter | string | `"+"` | https://www.postfix.org/postconf.5.html#recipient_delimiter |
| postfix.relayHost | string | `""` | https://www.postfix.org/postconf.5.html#relayhost |
| postfix.relayNets | string | `""` | will be added to mynetworks https://www.postfix.org/postconf.5.html#mynetworks, (e.g. `10.1.0.0/24 10.2.0.0/24`) be aware that mail from these networks can be sent without authentication! |
| postfix.rspamdHost | string | `""` | if rspamd is used, this should be set to the rspamd host and port, e.g. `rspamd.mydomain:10026` |
| postfix.smtpHostLookup | string | `"native"` | https://www.postfix.org/postconf.5.html#smtp_host_lookup), default `native` (postfix default is `dns`) |
| postfix.smtpSASLAuthEnable | string | `""` | https://www.postfix.org/postconf.5.html#smtp_sasl_auth_enable, default `no` |
| postfix.smtpSASLPasswordMaps | string | `""` | https://www.postfix.org/postconf.5.html#smtp_sasl_password_maps |
| postfix.smtpSASLSecurityOptions | string | `""` | https://www.postfix.org/postconf.5.html#smtpd_sasl_security_options |
| postfix.smtpTLSCAFile | string | `""` | https://www.postfix.org/postconf.5.html#smtp_tls_CAfile, default `/etc/ssl/certs/ca-certificates.crt` |
| postfix.smtpTLSSecurityLevel | string | `"encrypt"` | https://www.postfix.org/postconf.5.html#smtp_tls_security_level, default `encrypt` |
| postfix.smtpdKeyFile | string | `""` | https://www.postfix.org/postconf.5.html#smtpd_tls_key_file |
| postfix.smtpdMilters | string | `""` | https://www.postfix.org/postconf.5.html#smtpd_milters |
| postfix.smtpdSASLAuthEnable | string | `""` | https://www.postfix.org/postconf.5.html#smtpd_sasl_auth_enable, default `no` |
| postfix.smtpdSASLPath | string | `"smtpd"` | https://www.postfix.org/postconf.5.html#smtpd_sasl_path |
| postfix.smtpdSASLSecurityOptions | string | `""` | https://www.postfix.org/postconf.5.html#smtpd_sasl_security_options |
| postfix.smtpdSASLType | string | `""` | https://www.postfix.org/postconf.5.html#smtpd_sasl_type |
| postfix.smtpdTLSCertFile | string | `""` | https://www.postfix.org/postconf.5.html#smtpd_tls_cert_file |
| postfix.smtpdTLSSecurityLevel | string | `"encrypt"` | https://www.postfix.org/postconf.5.html#smtpd_tls_security_level, default `encrypt` |
| postfix.staticAuthDB.enabled | bool | `false` |  |
| postfix.staticAuthDB.password | string | `""` |  |
| postfix.staticAuthDB.username | string | `""` |  |
| postfix.transportMaps | string | `""` | https://www.postfix.org/postconf.5.html#transport_maps this map is used for the global postfix to transport mails into the corresponding namespaces |
| postfix.virtualMailboxDomains | string | `""` | - https://www.postfix.org/postconf.5.html#virtual_mailbox_domains |
| postfix.virtualTransport | string | `"virtual"` | https://www.postfix.org/postconf.5.html#virtual_transport, default `virtual` Postfix doesn't like an empty virtualTransport setting. |
| readinessProbe.enabled | bool | `true` | Enables kubernetes ReadinessProbe. |
| readinessProbe.failureThreshold | int | `10` | Number of failed executions until container is terminated. |
| readinessProbe.initialDelaySeconds | int | `15` | Delay after container start until ReadinessProbe is executed. |
| readinessProbe.periodSeconds | int | `20` | Time between probe executions. |
| readinessProbe.successThreshold | int | `1` | Number of successful executions after failed ones until container is marked healthy. |
| readinessProbe.timeoutSeconds | int | `5` | Timeout for command return. |
| replicaCount | int | `1` | Set the amount of replicas of deployment. |
| resources.limits.cpu | int | `4` | The max amount of CPUs to consume. |
| resources.limits.memory | string | `"4Gi"` | The max amount of RAM to consume. |
| resources.requests.cpu | string | `"10m"` | The amount of CPUs which has to be available on the scheduled node. |
| resources.requests.memory | string | `"2Gi"` | The amount of RAM which has to be available on the scheduled node. |
| service.annotations | object | `{}` | Additional custom annotations |
| service.enabled | bool | `true` | Enable kubernetes service creation. |
| service.external.enabled | bool | `false` | Wether postfix shall be reachable publicly |
| service.external.externalTrafficPolicy | string | `"Cluster"` | 'Local' (default) which means that the traffic is only routed to endpoints within the same node, preserving the original source IP. Cluster traffic can be routed to any endpoint within the cluster |
| service.external.internalTrafficPolicy | string | `"Cluster"` | 'Cluster' (default) traffic can be routed to any endpoint within the cluster, and 'Local' which means that the traffic is only routed to endpoints within the same node. |
| service.external.ports | object | `{"smtp":{"port":25},"smtps":{"port":587}}` | If the type is NodePort, exposed Ports and the corresponding nodePorts can be defined, e.g. For global Postfix: smtp:   port: 25   nodePort: 30025 |
| service.external.type | string | `"LoadBalancer"` | How to expose postfix, choices can be LoadBalancer or NodePort |
| service.ports | object | `{"smtp":{"port":25},"smtps":{"port":587}}` | All ports to expose internally via the postfix service: e.g. smtp:   port: 25 |
| service.sessionAffinity.enabled | bool | `false` | Wether session affinity should be enabled |
| serviceAccount.annotations | object | `{}` | Additional custom annotations for the ServiceAccount. |
| serviceAccount.automountServiceAccountToken | bool | `true` | Allows auto mount of ServiceAccountToken on the serviceAccount created. Can be set to false if pods using this serviceAccount do not need to use K8s API. |
| serviceAccount.create | bool | `true` | Enable creation of ServiceAccount for pod. |
| serviceAccount.labels | object | `{}` | Additional custom labels for the ServiceAccount. |
| terminationGracePeriodSeconds | string | `""` | In seconds, time the given to the pod needs to terminate gracefully. Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod/#termination-of-pods |
| tolerations | list | `[]` | Tolerations for pod assignment Ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/ |
| topologySpreadConstraints | list | `[]` | Topology spread constraints rely on node labels to identify the topology domain(s) that each Node is in Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-topology-spread-constraints/  topologySpreadConstraints:   - maxSkew: 1     topologyKey: failure-domain.beta.kubernetes.io/zone     whenUnsatisfiable: DoNotSchedule |
| updateStrategy.type | string | `"Recreate"` | Set to Recreate if you use persistent volume that cannot be mounted by more than one pods to make sure the pods is destroyed first. |

## Making Postfix available on the Internet

Basically, port 25 needs to be reachable on a public IP and a MX record needs to point to that IP. If email should also be sent through MUAs (e.g. Thunderbird), port 587 needs to be reachable as well.
Currently, there are 2 options supported: `LoadBalanacer` and `NodePort`. Depending on your cluster/network setup, there might be also other options which are not described in this readme.
After setting the service up, make sure to add the correct MX records for every domain.

### Option 1: service of type `LoadBalancer`

This requires a cloud setup that enables a Loadbalancer attachement. This could be enabled via values:

```yaml
service:
  external:
    enabled: true
    type: "LoadBalancer"
    ports:
      smtp:
        port: 25
      smtps:
        port: 587
```

### Option 2: NodePort with NAT (Network Address Translation) and PAT (Port Address Translation)

This setup requires an external firewall or loadbalancer with a public IP and port translation from the standard ports (25 and 587) on the external IP to the corresponding NodePorts on the internal Node IPs.

```yaml
service:
  external:
    enabled: true
    type: "NodePort"
    ports:
      smtp:
        port: 25
        nodePort: 30025
      smtps:
        port: 587
        nodePort: 30587
```

## Uninstalling the Chart

To install the release with name `my-release`:

```bash
helm uninstall my-release
```

## Signing

Helm charts are signed with helm native signing method.

You can verify the chart against [the public GPG key](../../files/gpg-pubkeys/opendesk.gpg).

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright (C) 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
